package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

public interface IWorker {

    abstract void run();

    void sayGoodbye(String workername);

    Semaphore getSemPing();

    Semaphore getSemPong();

    Semaphore getSemViewer();

    void setRunnable(boolean runnable);

}
