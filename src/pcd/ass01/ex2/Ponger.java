package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

/**
 * a model for ponger
 * 
 * @author Federica
 *
 */
public class Ponger extends Worker {
    private ManagerThread counter;

    public Ponger(Semaphore semPing, Semaphore semPong, Semaphore semViewer, Semaphore semMutex, ManagerThread counter) {
        super(semPing, semPong, semViewer, semMutex);
        this.counter = counter;

    }

    public synchronized void run() {

        while (this.isRunnable()) {
            try {
                this.getSemPong().acquire();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            // inizio mutex
            try {
                this.getsemMutex().acquire();
                try {
                    this.counter.setCount();
                } finally {
                    this.getsemMutex().release();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // fine mutex
            System.out.println("ponger -> pong!");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            this.getSemViewer().release();
        }
        

        try {
            this.getSemPong().acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        if(counter.getLastWorkedStarted().equals("pinger")){      
            this.sayGoodbye("ponger");
            this.getSemPing().release();
        }else if(counter.getLastWorkedStarted().equals("ponger")){
            this.sayGoodbye("ponger");
            this.getSemViewer().release();
        }else if(counter.getLastWorkedStarted().equals("none")){
            this.sayGoodbye("ponger");
            this.getSemViewer().release();
        }
        
    }

}
