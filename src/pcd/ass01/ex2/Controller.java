package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

public class Controller {

    private static Controller SINGLETON = null;

    private Semaphore semMutex = new Semaphore(1);
    private Semaphore semPing = new Semaphore(0);
    private Semaphore semPong = new Semaphore(0);
    private Semaphore semViewer = new Semaphore(0);
    private ManagerThread counter = new ManagerThread(0);

    // creo il viewer qui nel controller e poi lo mando a pinger e ponger per
    private Viewer viewer = new Viewer(this.semPong, this.semViewer, counter, this.semPing);  
    private Pinger pinger = new Pinger(this.semPing, this.semPong, this.semViewer, this.semMutex, counter);
    private Ponger ponger = new Ponger(this.semPing, this.semPong, this.semViewer, this.semMutex, counter);

    /**
     * singleton thread safe per il controller
     * 
     * @return
     */
    public static Controller getController() {
        if (SINGLETON == null) {
            synchronized (Controller.class) {
                if (SINGLETON == null) {
                    SINGLETON = new Controller();
                }
            }
        }
        return SINGLETON;
    }

    /**
     * avvio i tre thread
     */
    public void start() {
        this.pinger.start();
        this.viewer.start();
        this.ponger.start();

    }

    /**
     * stoppo i tre thread
     */
    public void stop() {
        this.pinger.stopThread();
        this.viewer.stopThread();
        this.ponger.stopThread();
    }

}
