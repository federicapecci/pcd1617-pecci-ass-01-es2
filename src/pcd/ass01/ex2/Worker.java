package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

public class Worker extends Thread implements IWorker {

    private Semaphore semPing;
    private Semaphore semPong;
    private Semaphore semViewer;
    private Semaphore semMutex;
    private boolean runnable;

    public Worker(Semaphore semPing, Semaphore semPong, Semaphore semViewer, Semaphore semMutex) {
        this.semPing = semPing;
        this.semPong = semPong;
        this.semViewer = semViewer;
        this.semMutex = semMutex;
        this.runnable = true;
    }

    @Override
    public void sayGoodbye(String workername) {
        System.out.println(workername + " -> Goodbye!");
    }

    /**
     * @return the semPing
     */
    public Semaphore getSemPing() {
        return semPing;
    }

    /**
     * @return the semPong
     */
    public Semaphore getSemPong() {
        return semPong;
    }

    /**
     * @return the semMutex
     */
    public Semaphore getsemMutex() {
        return semMutex;
    }

    /**
     * @return the semViewer
     */
    public Semaphore getSemViewer() {
        return semViewer;
    }

    public void stopThread() {
        this.runnable = false;
    }

    /**
     * @return the runnable
     */
    public boolean isRunnable() {
        return runnable;
    }

    /**
     * @param runnable
     *            the runnable to set
     */
    public void setRunnable(boolean runnable) {
        this.runnable = runnable;
    }

}
