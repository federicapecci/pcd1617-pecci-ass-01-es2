package pcd.ass01.ex2;

public class ManagerThread {

    private int count;
    private String lastWorkedStarted;

    public ManagerThread(int count) {
        this.count = count;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count
     *            the count to set
     */
    public void setCount() {
        this.count++;
    }
    
    public void setLastWorkedStarted(String lastWorkedStarted){
        this.lastWorkedStarted = lastWorkedStarted;
    }
    
    public String getLastWorkedStarted(){
        return this.lastWorkedStarted;
    }

}
