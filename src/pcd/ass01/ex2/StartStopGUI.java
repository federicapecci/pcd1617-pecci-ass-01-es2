package pcd.ass01.ex2;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class StartStopGUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public StartStopGUI(Controller controller) {

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Ping Pong");

        final Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        final JPanel startStop = new JPanel();
        final JButton start = new JButton("Start");
        final JButton stop = new JButton("Stop");

        this.setSize((int) (d.getWidth() / 2), (int) (d.getHeight() / 4));

        start.addActionListener(x -> controller.start());
        stop.addActionListener(x -> controller.stop());

        startStop.add(start);
        startStop.add(stop);

        this.getContentPane().add(startStop);
        this.setVisible(true);
    }

}
