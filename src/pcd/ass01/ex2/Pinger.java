package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

/**
 * a model for pinger
 * 
 * @author Federica
 *
 */
public class Pinger extends Worker {

    private ManagerThread counter;

    public Pinger(Semaphore semPing, Semaphore semPong, Semaphore semViewer, Semaphore semMutex, ManagerThread counter) {
        super(semPing, semPong, semViewer, semMutex);
        this.counter = counter;

    }

    public synchronized void run() {

        while (this.isRunnable()) {
            try {
                this.getsemMutex().acquire();
                try {
                    this.counter.setCount();
                } finally {
                    this.getsemMutex().release();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            };
            System.out.println("pinger -> ping");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            this.getSemViewer().release();
            try {
                this.getSemPing().acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
         
        try {
            this.getSemPing().acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        if(counter.getLastWorkedStarted().equals("pinger")){          
            this.sayGoodbye("pinger");
            this.getSemViewer().release();
        }else if(counter.getLastWorkedStarted().equals("ponger")){
            this.sayGoodbye("pinger");
            this.getSemPong().release();
        }else if(counter.getLastWorkedStarted().equals("none")){
            this.sayGoodbye("pinger");
            this.getSemPong().release();
        }
    }

}
