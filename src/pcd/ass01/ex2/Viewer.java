package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

public class Viewer extends Thread {

    private ManagerThread counter;
    private Semaphore semPing;
    private Semaphore semPong;
    private Semaphore semViewer;
    private boolean runnable;

    /**
     * per richiamare oggetti del controller dal model, glieli passo dal
     * costruttore
     * 
     * @param semPing
     * @param semPong
     * @param semViewer
     */
    public Viewer(Semaphore semPong, Semaphore semViewer, ManagerThread counter, Semaphore semPing) {
        this.semPing = semPing;
        this.semPong = semPong;
        this.semViewer = semViewer;
        this.counter = counter;
        this.runnable = true;

    }

    /**
     * this method contains logic for the Viewer thread
     */
    public synchronized void run() {

        while (this.runnable) {
            acquireAndSystem();
            this.semPong.release();
            acquireAndSystem();
            this.semPing.release();
        }
   
        System.out.println("viewer -> Goodbye!");
        
        if(this.semPing.hasQueuedThreads()){  
           counter.setLastWorkedStarted("pinger");
           this.semPong.release();
        }else if(this.semPong.hasQueuedThreads()){
            counter.setLastWorkedStarted("ponger");
            this.semPing.release();
        }else{
            counter.setLastWorkedStarted("none");
            this.semPing.release();
        }
        
        try {
            this.semViewer.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        try {
            this.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);

    }

    /**
     * this method needs to acquire semaphore and print counter value, The
     * pseudocode encapsulating is the following: P(viewer) System(counter)
     */
    private void acquireAndSystem() {

        try {
            semViewer.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("viewer -> " + this.counter.getCount());
        try {
            Thread.sleep(100);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

    }

    /**
     * @param runnable
     * the runnable to set
     */
    void setRunnable(boolean state) {
        this.runnable = state;
    }

    /**
     * 
     * @return runnable
     */
    public boolean getRunnable() {
        return this.runnable;
    }

    public void stopThread() {
        this.runnable = false;
    }

}
