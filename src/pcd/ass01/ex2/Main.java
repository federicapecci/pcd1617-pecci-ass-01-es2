package pcd.ass01.ex2;

public class Main {

    public static void main(String args[]) {
        Controller controller = Controller.getController();

        // passo il controller alla gui quando la creo
        new StartStopGUI(controller);

    }
}
